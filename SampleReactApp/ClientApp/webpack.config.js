const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const path = require("path");

module.exports = (env) => {
  return {
    context: __dirname,
    entry: "./src/index.tsx",
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx", ".scss"],
    },
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "index_bundle.[contenthash:15].js",
      publicPath: "/",
    },
    devServer: {
      historyApiFallback: true,
      compress: true,
      port: 8066,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: "babel-loader",
        },
        {
          test: /\.ts$/,
          exclude: /node_modules/,
          use: "ts-loader",
        },
        {
          test: /\.tsx$/,
          exclude: /node_modules/,
          use: "ts-loader",
        },
        {
          test: /\.css$/,
          // exclude: /node_modules/,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.scss$/,
          // exclude: /node_modules/,
          // include: [path.resolve(__dirname, 'src', 'sass')],
          use: [
            "style-loader",
            "css-loader",
            "stylus-loader",
            "postcss-loader",
            "sass-loader",
          ],
        },
        {
          test: /\.(png|svg|jpg|jpe?g|gif|mp3)$/,
          // use: 'file-loader?name=./images/[name].[ext]',
          use: [
            {
              loader: "file-loader",
              options: {
                esModule: false,
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "public/index.html"),
        filename: "index.html",
      }),
      new Dotenv(),
    ],
  };
};

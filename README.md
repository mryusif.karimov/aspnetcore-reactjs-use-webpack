# Asp.Net Core ReactJs Use Webpack

This is sample app about  how can we use webpack for webapp which that created with AspNet Core 5 with ReactJs (with Redux store) template

## Getting started
#### Firstly we need some important npm packages which these help us we use webpack fully.
#### Also we must configure 4 files if we create our web app with VS 2019 (doesn't matter which version)
#### In this example I checked Asp.Net Core 5 WebApi  with Reactjs Redux-Store

## All needed npm dev packages
##### webpack 
##### webpack-cli 
##### webpack-dev-server 
##### html-webpack-plugin
##### dotenv-webpack  
##### @babel/core 
##### @babel/plugin-proposal-class-properties 
##### @babel/preset-env  
##### @babel/preset-react 
##### babel-loader 
##### css-loader 
##### style-loader 
##### file-loader 
##### ts-loader 
##### raw-loader 
##### sass-loader 
##### sass 
##### postcss-loader 
##### postcss 
##### stylus 
##### stylus-loader

# All needed npm local packages
##### @babel/runtime

# All needed configure files
##### .env
##### webpack.config.js
##### tsconfig.json
##### .babelrc

# Added commands for star, build, deploy
    "webpack-dev-server": "webpack-dev-server"
    "dev": "webpack serve --mode=development"
    "prod": "NODE_ENV=production webpack --mode=development"
    "serve": "rimraf dist && npm run prod && serve -s dist"